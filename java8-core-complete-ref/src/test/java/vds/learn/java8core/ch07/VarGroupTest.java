package vds.learn.java8core.ch07;


import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.sql.Array;

import static org.fest.assertions.api.Assertions.assertThat;

public class VarGroupTest {

    @Test
    public void shouldBeVarArgsRepresentsAsArray() {

        int[] expectedVarArgs = {1, 2, 3};
        assertThat(someMethodWithVarArgs(1, 2, 3)).isEqualTo(expectedVarArgs);
    }

    private int[] someMethodWithVarArgs(int... a) {
        return a;
    }
}
