package vds.learn.java8core.ch12;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;

import static org.fest.assertions.api.Assertions.assertThat;

/**
 * This junit class represents
 */
public class AnnotationExampleTest {

    private ClassWithAnnotating classWithAnnotating;
    private Class<?> testedClass;

    class ClassWithAnnotating {
        @SimpleAnnotation(Description = "myDescription", initValue = 100)
        public void fo() {
        }
    }

    @Before
    public void setup() {
        classWithAnnotating = new ClassWithAnnotating();
        testedClass = classWithAnnotating.getClass();
    }

    @Test
    public void shouldGetPropeverValueOfAnnotation() throws NoSuchMethodException {
        Method method = testedClass.getMethod("fo");
        SimpleAnnotation ann = method.getDeclaredAnnotation(SimpleAnnotation.class);

        assertThat(ann.initValue()).isEqualTo(100);
    }

    @Test
    public void shouldGetPropererValueDefaultValueForStepValue() throws NoSuchMethodException {
        Method method = testedClass.getMethod("fo");
        SimpleAnnotation ann = method.getDeclaredAnnotation(SimpleAnnotation.class);

        assertThat(ann.stepsValue()).isEqualTo(10);
    }

    @Test
    public void shouldCheckIfAdnotationisAvaiable() throws NoSuchMethodException {
        Method method = testedClass.getMethod("fo");
        SimpleAnnotation ann = method.getDeclaredAnnotation(SimpleAnnotation.class);

        assertThat(method.isAnnotationPresent(SimpleAnnotation.class)).isTrue();
    }
}
