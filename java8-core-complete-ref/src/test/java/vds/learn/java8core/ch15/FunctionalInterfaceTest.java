package vds.learn.java8core.ch15;

import org.junit.Test;

import java.util.function.Function;

import static org.fest.assertions.api.Assertions.assertThat;

public class FunctionalInterfaceTest {

    @Test
    public void shouldExecutePowerOperationUsingFunctionalInterface() {
        FunInterfaceValue powerOperation = n -> n * n;
        assertThat(powerOperation.getValue(3));
    }

    @Test
    public void shouldExecutePowerOperationUsingLambaExpAndFunctionInterface() {
        Function<Integer, Integer> powerOperation = n -> n * n;
        assertThat(powerOperation.apply(2));
    }

    @Test
    public void shouldExecuteFunctionalUsingGenericInterface() {
        GenericFunctionalInterface<Integer> fn = n -> n * n;
        assertThat(fn.apply(3)).isEqualTo(9);
    }

    int someMethodWithWithFunctionalArgs(Function<Integer, Integer> fn, int n) {
        return fn.apply(n);
    }

    @Test
    public void shouldUsesFunctionAsPartOfAgrument() {
        int res = someMethodWithWithFunctionalArgs(n -> n + 3, 3);
        assertThat(res).isEqualTo(6);
    }

    int someMethodCalculatePower(int n) {
        return n * n;
    }

    @Test
    public void shouldConvertMethodToFunctionAndRunIt() {
        Function<Integer, Integer> fn = this::someMethodCalculatePower;
        assertThat(fn.apply(2)).isEqualTo(4);
    }
}