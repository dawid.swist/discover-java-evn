package vds.learn.java8core.ch09;

import org.junit.Test;

import static org.fest.assertions.api.Assertions.assertThat;

public class DefaultMethodTest {
    @Test
    public void shouldReturnStringFromDefaultMethod() {
        SomeCLassWithInterface someCLassWithInterface = new SomeCLassWithInterface();

        assertThat(someCLassWithInterface.method())
                .isEqualTo(FROM_SOME_INTERFACE_WITH_DEFAULT_METHOD_METHOD);
    }

    @Test
    public void shouldReturnStringFromClass() {

        SomeCLassWithInterfaceAndOverwriteing someCLassWithInterfaceAndOverwriteing =
                new SomeCLassWithInterfaceAndOverwriteing();

        assertThat(someCLassWithInterfaceAndOverwriteing.method()).isEqualTo(FROM_CLASS_SOME_CLASS_WITH_INTERFACE_AND_OVERWRITEING);
    }

    private static final String FROM_CLASS_SOME_CLASS_WITH_INTERFACE_AND_OVERWRITEING = "FROM class SomeCLassWithInterfaceAndOverwriteing";
    private static String FROM_SOME_INTERFACE_WITH_DEFAULT_METHOD_METHOD = "from SomeInterfaceWithDefaultMethod.method()";

    interface SomeInterfaceWithDefaultMethod {
        default String method() {
            return FROM_SOME_INTERFACE_WITH_DEFAULT_METHOD_METHOD;
        }
    }

    private class SomeCLassWithInterface implements SomeInterfaceWithDefaultMethod {
    }

    private class SomeCLassWithInterfaceAndOverwriteing implements SomeInterfaceWithDefaultMethod {
        @Override
        public String method() {
            return FROM_CLASS_SOME_CLASS_WITH_INTERFACE_AND_OVERWRITEING;
        }
    }
}