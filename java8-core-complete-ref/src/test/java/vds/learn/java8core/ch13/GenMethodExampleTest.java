package vds.learn.java8core.ch13;

import org.junit.Test;

import static org.fest.assertions.api.Assertions.assertThat;


/**
 * Created by swistdaw on 2016-06-06.
 */
public class GenMethodExampleTest {
    @Test
    public void shouldApplyGenericMethod() throws Exception {

        assertThat(GenMethodExample.someGenMethod(200, "200")).isTrue();
    }

}