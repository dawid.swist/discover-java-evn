package vds.learn.java8core.ch15;

/**
 * Example of functional interface for operation int (x) -> ...
 */
interface FunInterfaceValue {
    int getValue(int n);
}
