package vds.learn.java8core.ch15;

/**
 * Example of generic functional interface
 *
 * @param <T>
 */
public interface GenericFunctionalInterface<T> {
    T apply(T n);
}
