package vds.learn.java8core.ch13;

public class Gen<T> {
    private T ob;

    T getOb() {
        return ob;
    }

    public void setOb(T ob) {
        this.ob = ob;
    }
}
