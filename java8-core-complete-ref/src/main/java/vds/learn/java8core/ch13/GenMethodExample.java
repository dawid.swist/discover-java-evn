package vds.learn.java8core.ch13;

/**
 * This class show how to use generic in method.
 */
public class GenMethodExample {
    /**
     * Method check if some t number is eq v value writen as string
     *
     * @param t
     * @param v
     * @param <T>
     * @param <V>
     * @return
     */
    static <T extends Number, V extends String> boolean someGenMethod(T t, V v) {
        return v.equals(t.toString());
    }

}
