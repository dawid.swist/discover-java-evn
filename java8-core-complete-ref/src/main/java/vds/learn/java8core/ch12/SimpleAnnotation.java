package vds.learn.java8core.ch12;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Example of simple annotation
 */
@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = ElementType.METHOD)
@interface SimpleAnnotation {

    String Description();

    int initValue();

    int stepsValue() default 10;
}
